#!/bin/bash

## This script has as propose to create a playlist in XSPF format that could
## work as VLC media library.

# How to run:
# ./vlc-media-library.sh /home/neo-cs/tmp

function create_logger_file() {
  local directory=$1
  local current_date=$(date +"%Y%m%d-%H%M%S")
  logger=$directory/vlc-media-library__$current_date.log

  touch $logger
}

function copy_empty_library() {
  local destination=$1
  echo "Copying empty media library file" | tee -a $logger
  cp -v --remove-destination ../../resources/empty-media_library.xspf $destination/media_library.xspf 2>&1 | tee -a $logger
}

function find_music() {
  local destination=$1
  local directory=/home/neo-cs/Downloads/Music
  
  find $destination -type f -name lib.txt -delete
  find $directory/To\ sort/ -type f \( -iname '*.mp3' -o -iname '*.m4a' -o -iname '*.wav' -o -iname '*.wma' \) >> $destination/lib.txt
  find $directory/Library/ -type f \( -iname '*.mp3' -o -iname '*.m4a' -o -iname '*.wav' -o -iname '*.wma' \) >> $destination/lib.txt
  find $_USER_HOME_PATH/Music/Library/ -type f \( -iname '*.mp3' -o -iname '*.m4a' -o -iname '*.wav' -o -iname '*.wma' \) >> $destination/lib.txt
  echo "lib.txt: created" | tee -a $logger
  total_files=$(wc < $destination/lib.txt | awk '{print $1}')
  echo "Total audio files found: $total_files" | tee -a $logger
}

function url_encode() {
  local length="${#1}"

  for (( i = 0; i < length; i++ )); do
    local c="${1:i:1}"
    case $c in
      [a-zA-Z0-9.~_-] | \/) printf "$c" ;;
      %)                    printf "%%%s25" ;;
      *)                    printf "$c" | xxd -p -c1 | while read x; do printf "%%%s" "$x"; done
    esac
  done
}

function add_to_media_library() {
  local destination=$1
  local media_library=$destination/media_library.xspf
  local count=0

  cat $destination/lib.txt | while read line || [[ -n $line ]];
  do
     echo "Adding: $line" | tee -a $logger

     local element=$(echo "<track><location>file://$(url_encode "$line")</location></track>" | sed 's/\//\\\//g' | sed 's/\&/\\&/g')
     echo "Value to add: $element" | tee -a $logger
     sed -i "/<\/trackList>/ s/.*/${element}\n&/" $media_library
     ((count++))
     echo "Count: $count/$total_files" | tee -a $logger
     printf "Completed: %.2f%%\n" "$(bc <<< "scale = 10;  (($count / $total_files) * 100)")" | tee -a $logger
  done
}

function create_backup() {
  local vlc_path=$_USER_HOME_PATH/.local/share/vlc
  local current_date=$(date +"%Y%m%d-%H%M%S")
  
  if [[ -f "$vlc_path" ]] && [[ -f "$vlc_path/ml.xspf" ]] 
  then
    echo "Creating backup" | tee -a $logger
    cp -v $vlc_path/ml.xspf $vlc_path/$current_date-backup--ml.xspf 2>&1 | tee -a $logger
  elif [[ -f "$vlc_path" ]]
    echo "Creating $vlc_path directory" | tee -a $logger
    mkdir -p $vlc_path
  else
    echo "There isn't anything to backup." | tee -a $logger
  fi
}

function replace_media_library() {
  local destination=$1
  local vlc_path=$_USER_HOME_PATH/.local/share/vlc
  
  echo "Replacing media library" 2>&1 | tee -a $logger
  find $vlc_path -type f -name ml.xspf -delete 2>&1 | tee -a $logger
  cp -v $destination/media_library.xspf $vlc_path/ml.xspf 2>&1 | tee -a $logger
}

_USER_HOME_PATH=$(eval echo ~$USER)

create_logger_file $1
copy_empty_library $1
find_music $1
add_to_media_library $1
create_backup
replace_media_library $1

exit 0

